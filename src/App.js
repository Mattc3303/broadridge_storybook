import './App.css';
import Button from './components/button/button.component';

function App() {
  return (
    <div className="App">
      <Button />
    </div>
  );
}

export default App;
