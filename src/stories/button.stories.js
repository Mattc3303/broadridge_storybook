import React from 'react';
import Button from '../components/button/button.component';

export default {
  component: Button,
  title: 'Button',
  parameters: {
    componentSubtitle: 'Displays an Generic Button',
  },
};

const Template = args => <Button {...args} />;

export const Default = Template.bind({});
Default.args = {
  backgroundColor: 'white',
  buttonChild: 'Click me!'
};

export const Red = Template.bind({});
Red.args = {
  backgroundColor: 'red'

};

export const Blue = Template.bind({});
Blue.args = {
  backgroundColor: 'blue'
};