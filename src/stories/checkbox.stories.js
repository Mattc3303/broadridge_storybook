import React from 'react';
import Checkbox from '../components/checkbox/checkbox.component';

export default {
  title: 'Checkbox',
  component: Checkbox,
};

const Template = args => <Checkbox {...args} />;

export const Default = Template.bind({});