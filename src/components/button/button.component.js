import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ 
  backgroundColor = 'white',
  buttonChild = '',
}) => (
  <button style={{ 
    background: backgroundColor,
    padding: '5px', 
    minWidth: '60px',
    width: 'auto', 
    height: '30px', 
  }}>
    {buttonChild}
  </button>
);

Button.propTypes = {
  backgroundColor: PropTypes.string,
  /**
    Takes a type string or jsx as a child of component Button
  */
  buttonChild: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
}

export default Button;