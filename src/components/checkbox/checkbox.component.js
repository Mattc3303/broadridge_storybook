import React from 'react';
import PropTypes from 'prop-types';

const Checkbox = ({
  disabled = false
}) => (
  <input type='text' disabled={disabled}/>
);

Checkbox.propTypes = {
  disabled: PropTypes.bool,
}

export default Checkbox;