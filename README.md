## Goal: Providing better code documentation for existing storybook components

### Option 1: Using story books own automated docs generation

### Option 2: Using story books own docs generation but replacing with your own MDX file
- https://storybook.js.org/docs/react/writing-docs/doc-blocks

### Research
- https://storybook.js.org/docs/react/writing-docs/docs-page

### Running localhost
`npm run storybook`

### Generating Prod 
`npm run build-storybook -- -o ./build_sb`  
